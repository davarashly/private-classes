public class Calc {
    private double x;

    public Calc(double x) {
        this.x = x;
    }

    public Calc() {
        this.x = 1;
    }

    void addParameter(double a) {
        x += a;
    }

    void subParameter(double a) {
        x -= a;
    }

    void multParameter(double a) {
        x *= a;
    }

    void divParameter(double a) {
        x /= a;
    }

    void sqrtx() {
        x = Math.sqrt(x);
    }

    void sinx() {
        x = Math.sin(x);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }
}

class CalcY extends Calc {

    private double y;

    public CalcY() {
        super();
        this.y = 1;
    }

    public CalcY(double y) {
        super();
        this.y = y;
    }

    public CalcY(double x, double y) {
        super(x);
        this.y = y;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    void addXToY() {
        this.setY(this.getY() + this.getX());
    }
}
