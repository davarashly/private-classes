public class Fraction_task {
    public static void main(String[] args) {
        Fraction fraction = new Fraction(1, 97);
        Fraction fraction1 = new Fraction(1, 512);
        FractionLogic logic = new FractionLogic(fraction, fraction1);
        fraction.printFraction();
        System.out.println();
        fraction1.printFraction();
        System.out.println("***************************");
        System.out.println();
        logic.addFraction().printFraction();
        System.out.println();
        logic.subFraction().printFraction();
        System.out.println();
        logic.multFraction().printFraction();
        System.out.println();
        logic.divFraction().printFraction();
    }
}

class Fraction {
    private int numerator;
    private int denominator;
    private int integer;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        this.integer = 0;
        this.reduceFraction();
    }

    public Fraction() {
        this.numerator = 0;
        this.denominator = 0;
        this.integer = 0;
    }

    public void intToFraction() {
        if (numerator == 0 && denominator == 0) {
            numerator = integer;
            denominator = 1;
            integer = 0;
        }
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void printFraction() {
        int max = Integer.toString(numerator).length() > Integer.toString(denominator).length() ? Integer.toString(numerator).length() : Integer.toString(denominator).length();

        if (numerator == 0 && denominator == 0)
            System.out.println(integer);
        else if (integer < 1) {
            System.out.println(StringUtils.center(Integer.toString(numerator), max));
            for (int i = 0; i < max; i++)
                System.out.print("—");
            System.out.println();
            System.out.println(StringUtils.center(Integer.toString(denominator), max));
        } else {
            int space = Integer.toString(integer).length() + 1;

            System.out.println();
            for (int i = 0; i < space; i++)
                System.out.print(" ");
            System.out.print(StringUtils.center(Integer.toString(numerator), max) + "\n");

            System.out.print(integer + " ");

            for (int i = 0; i < max; i++)
                System.out.print("—");
            System.out.println();
            for (int i = 0; i < space; i++)
                System.out.print(" ");
            System.out.print(StringUtils.center(Integer.toString(denominator), max) + "\n");

        }
    }

    public void reduceFraction() {
        int nod, a = numerator, b = denominator;
        while (b > 0 && a > 0) {
            int tmp = a;
            a = b;
            b = tmp % b;
        }
        nod = a;
        if (nod > 0) {
            numerator /= nod;
            denominator /= nod;
        }
        while (numerator > denominator) {
            if (numerator % denominator == 0) {
                integer = numerator / denominator;
                numerator = 0;
                denominator = 0;
            } else {
                numerator -= denominator;
                integer++;
            }
        }
    }
}

class FractionLogic {
    Fraction fraction;
    Fraction fraction1;

    public FractionLogic(Fraction fraction, Fraction fraction1) {
        this.fraction = fraction;
        this.fraction1 = fraction1;
    }

    public Fraction addFraction() {
        Fraction sum = new Fraction();
        fraction.intToFraction();
        fraction1.intToFraction();
        sum.setNumerator(fraction.getNumerator() * fraction1.getDenominator() + fraction1.getNumerator() * fraction.getDenominator());
        sum.setDenominator(fraction.getDenominator() * fraction1.getDenominator());
        sum.reduceFraction();

        return sum;
    }

    public Fraction subFraction() {
        fraction.intToFraction();
        fraction1.intToFraction();
        Fraction sub = new Fraction();
        sub.setNumerator(fraction.getNumerator() * fraction1.getDenominator() - fraction1.getNumerator() * fraction.getDenominator());
        sub.setDenominator(fraction.getDenominator() * fraction1.getDenominator());
        sub.reduceFraction();

        return sub;
    }

    public Fraction multFraction() {
        fraction.intToFraction();
        fraction1.intToFraction();
        Fraction mult = new Fraction();
        mult.setNumerator(fraction.getNumerator() * fraction1.getNumerator());
        mult.setDenominator(fraction.getDenominator() * fraction1.getDenominator());
        mult.reduceFraction();

        return mult;
    }

    public Fraction divFraction() {
        fraction.intToFraction();
        fraction1.intToFraction();
        Fraction div = new Fraction();
        div.setNumerator(fraction.getNumerator() * fraction1.getDenominator());
        div.setDenominator(fraction.getDenominator() * fraction1.getNumerator());
        div.reduceFraction();

        return div;
    }
}
